FROM node:12-buster
RUN apt-get update && apt-get install -y curl brotli && curl -L https://unpkg.com/@pnpm/self-installer | node
